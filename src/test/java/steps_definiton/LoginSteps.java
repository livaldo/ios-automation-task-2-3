package steps_definiton;

import page_object.LoginPage;
import io.cucumber.java8.En;
public class LoginSteps implements En{

    LoginPage loginPage = new LoginPage();
    public LoginSteps() {
        Given("^User click next button track app$", () ->  loginPage.tapNextButtonAllowTracking());

        When("^User see entry point login$", () ->  loginPage.isEntryPointLoginButton());

        And("^User click entry point login$", () -> loginPage.tapEntryPointLoginButton());

        And("^User input username as \"([^\"]*)\"$", (String username) -> loginPage.inputFieldUsername(username));

        And("^User input password as \"([^\"]*)\"$", (String password) -> loginPage.inputFieldPassword(password));

        And("^User click button login$", () -> loginPage.tapLoginButton());

        Then("^User see watchlist page$", () -> loginPage.isWatchlistPage());

        Then("^User see error message$", () -> loginPage.isFailedLogin());

    }
}
