package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import ios_driver.IOSDriverInstance;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import utils.Constans;
import utils.Utils;


public class IOSDriverHooks {
    @Before(value = "@iOS")
    public void initializeIOSDriver(){
        IOSDriverInstance.initialize();
        Utils.loadElementProperties(Constans.ELEMENTS);
    }
    @After(value = "@iOS")
    public void quitDriver(Scenario scenario) {
        boolean testStatus = scenario.isFailed();
        try{
            if (testStatus){
                final byte[] data =((TakesScreenshot) IOSDriverInstance.iosDriver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(data, "image/png", "Failed Screenshot");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOSDriverInstance.quit();
        }
    }
}
