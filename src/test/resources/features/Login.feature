@iOS @LoginFeature
  Feature: [iOS] Login Feature

    @iOS @SuccessLogin
      Scenario: [iOS] User success login stockbit
      Given User click next button track app
      When User see entry point login
      And User click entry point login
      And User input username as "USERNAME_SOCIAL"
      And User input password as "PASSWORD_SOCIAL"
      And User click button login
      Then User see watchlist page

    @iOS @FailedLoginUsingUsername
    Scenario: [iOS] User failed login stockbit with invalid username
      Given User click next button track app
      When User see entry point login
      And User click entry point login
      And User input username as "WRONG_USERNAME_SOCIAL"
      And User input password as "PASSWORD_SOCIAL"
      And User click button login
      Then User see error message

    @iOS @InvalidLoginWithWrongPassword
    Scenario: [iOS] Invalid login with wrong password
      Given User click next button track app
      When User see entry point login
      And User click entry point login
      And User input username as "USERNAME_SOCIAL"
      And User input password as "WRONG_USERNAME_PASSWORD"
      And User click button login
      Then User see error message

    @iOS @InvalidLoginWrongUsernamePassword
    Scenario: [iOS] Invalid login with wrong username and password
      Given User click next button track app
      When User see entry point login
      And User click entry point login
      And User input username as "WRONG_USERNAME_SOCIAL"
      And User input password as "WRONG_USERNAME_SOCIAL"
      And User click button login
      Then User see error message
