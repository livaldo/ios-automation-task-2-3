package ios_driver;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.options.XCUITestOptions;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import static utils.Constans.TIMEOUT;
import static utils.Utils.env;

public class IOSDriverInstance {
    public static IOSDriver iosDriver;
    public static String errorMessage;

    public static void initialize(){
        XCUITestOptions caps = new XCUITestOptions();

        caps.setPlatformName(env("PLATFORM_NAME"));
        caps.setPlatformVersion(env("PLATFORM_VERSION"));
        caps.setDeviceName(env("DEVICE_NAME"));
        caps.setApp(env("APP"));
        //caps.autoAcceptAlerts(); di command karna di 16.4 blocker, kalau butuh bisa dinyalain

        try{
            iosDriver = new IOSDriver(new URL(env("APPIUM_URL")), caps);
            iosDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(TIMEOUT));
        } catch (MalformedURLException e){
            throw new RuntimeException(e);
        }
    }

    public static void quit (){
        iosDriver.quit();
    }
}
