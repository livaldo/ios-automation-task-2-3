package page_object;

import static utils.Utils.env;

public class LoginPage extends BasePageObjects {

    public void tapNextButtonAllowTracking() {
        tap("BUTTON_NEXT_PERMISSION");
    }

    public void isEntryPointLoginButton() {
        assertIsDisplay("BUTTON_WELCOME_PAGE");
    }

    public void tapEntryPointLoginButton() {
        tap("BUTTON_WELCOME_PAGE");
    }
    public void inputFieldUsername(String username) {
        typeOn("FIELD_USERNAME_LOGIN", env(username));
    }

    public void inputFieldPassword(String password) {
        typeOn("FIELD_PASSWORD_LOGIN", env (password));
    }

    public void tapLoginButton() {

        tap("BUTTON_LOGIN");
       // tap("BUTTON_SKIP_SMART_LOGIN");
       // tap("BUTTON_SKIP_AVATAR");
    }

    public void isWatchlistPage() {
        assertIsDisplay("VIEW_WATCHLIST_PAGE");
    }

    public void isFailedLogin() throws InterruptedException {
        assertIsDisplay("ERROR_LOGIN_MESSAGE");
        Thread.sleep(3000);
    }
}
