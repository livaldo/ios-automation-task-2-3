package page_object;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.ios.IOSDriver;
import ios_driver.IOSDriverInstance;
import org.asynchttpclient.netty.timeout.TimeoutTimerTask;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import static utils.Constans.TIMEOUT;
import static utils.Utils.ELEMENTS;

public class BasePageObjects {
    public IOSDriver driver() {
        return IOSDriverInstance.iosDriver;
    }
    public WebElement waitUntil(ExpectedCondition<WebElement> conditions, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver(), Duration.ofSeconds(timeout));
        return wait.until(conditions);
    }
    public WebElement waitUntilClickable(By by)
    {
        return waitUntil(ExpectedConditions.elementToBeClickable(by),TIMEOUT);
    }

    public WebElement waitUntilDisplayed(By by){
        return waitUntil(ExpectedConditions.presenceOfElementLocated(by),TIMEOUT);
    }

    public WebElement waitUntilVisible(By by){
        return waitUntil(ExpectedConditions.visibilityOfElementLocated(by), TIMEOUT);
    }

    public void tap(By by) {
        waitUntilClickable(by).click();
    }

    public void typeOn(By by, String value) {
        waitUntilDisplayed(by).sendKeys(value);
    }

    public void assertIsDisplayed(By by){
        waitUntilVisible(by).isDisplayed();
    }

    public void assertIsVisible(By by){
        waitUntilVisible(by).isDisplayed();
    }

    public void assertIsDisplay(By by) {
        try {
            driver().findElement(by ).isDisplayed();
        } catch (NoSuchElementException e) {
            throw new AssertionError(String.format("This element '%s' not found", by));
        }
    }

    public By element(String elementLocator){
        String elementValue = ELEMENTS.getProperty(elementLocator);
        if (elementValue == null) {
            printError("Couldn't find element :" + elementLocator + " ! Please check properties file!");
            throw new NoSuchElementException("Couldn't find element : " + elementLocator);
        }
            else {
                    String[] locator = elementValue.split("_");
                    String locatorType = locator[0];
                    String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);
                    return switch (locatorType) {
                    case "id" -> By.id(locatorValue);
                    case "name" -> By.name(locatorValue);
                    case "xpath" -> By.xpath(locatorValue);
                    case "containsText" -> By.xpath(String.format("//*[contains(@text, '%s')]", locatorValue));
                    case "cssSelector" -> By.cssSelector(locatorValue);
                    case "accessibilityId" -> AppiumBy.accessibilityId(locatorValue);
                    default -> throw new IllegalStateException("Unexpected locator type: " + locatorType);
            };
        }
    }
    public static void printError(String errorMessage) {
        IOSDriverInstance.errorMessage = errorMessage;
        throw new AssertionError(errorMessage);
    }
    public void tap(String element){
        tap(element(element));
    }
    public void assertIsDisplay(String element){
        assertIsDisplay(element(element));
    }

    public void assertIsVisible(String element){
        assertIsVisible(element(element));
    }
    public void typeOn(String element, String value){
        typeOn(element(element),value);
    }
}
