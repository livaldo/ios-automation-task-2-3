# Project ios-automation-bootcamp-task-2-3

This project contains automation test for Stockbit iOS bootcamp created by group 3. It is build using IntelliJ IDEA and requires some dependencies to be installed


## Dependencies

- Appium :
  Appium is an open-source test automation framework for mobile applications. It allows developers and testers to write tests for mobile applications (Android and iOS) using standard programming languages like Java, Python, C#, Ruby, and others. Appium supports both native mobile apps and mobile web apps.

- Cucumber : Cucumber is a testing tool that works in conjunction with testing frameworks, primarily used for behavior-driven development (BDD). The Cucumber framework enables the creation of test cases in a natural language format, which is then converted into executable code. This facilitates collaboration between technical and non-technical stakeholders in the software development process.

- Selenium : Selenium is an open-source framework for automating web browsers. It provides a way for developers and testers to write scripts in various programming languages to automate interactions with web applications.


## Setup Instruction and Tools

1. Install Homebrew (if not already installed):
   Open Terminal and run the following command:

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```


2. Install Node.js and npm:
   Run the following commands in Terminal:

```
brew install node
```


3. Install Appium:
   Run the following command to install Appium using npm:

```
npm install -g appium
```

4. Install Appium Doctor :
   Appium Doctor is a tool that checks your system for dependencies and helps ensure that everything is set up correctly. Run the following command:

```
npm install -g appium-doctor
```

Then, run Appium Doctor to check for any missing dependencies:

```
appium-doctor
```

5. Setup Cucumber :
   Open the build.gradle file and add the necessary dependencies for Cucumber and any other libraries you may need.

```
dependencies {
        implementation 'io.cucumber:cucumber-java:7.3.1'
        implementation 'io.cucumber:cucumber-junit:7.3.1'
        testImplementation 'junit:junit:4.13.2'
}
```

6. Setup JUnit
dependencies for JUnit:
```
dependencies {
    // Other dependencies
    testImplementation 'junit:junit:4.12' // Use the latest version available
}
```

7. Selenium
dependencies for Selenium
```
dependencies {
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.8.0' // Use the latest version available
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.8.0' // Use the latest version available
}
``` 
